/* eslint-disable global-require,import/no-unresolved */
import PropTypes from 'prop-types';
import React from 'react';
import { Keyboard, StyleSheet, View } from 'react-native';
import { Button, Form, Icon, Spinner, Text } from 'native-base';

import variables from 'eoffice/native-base-theme/variables/commonColor';
import { moderateScale } from 'eoffice/utils/scaling';
import Input from './Input';
import useLoginForm from './useLoginForm';

const styles = StyleSheet.create({
  btn: {
    position: 'absolute',
    top: moderateScale(15, 0.5),
    right: 0,
    height: moderateScale(48, 0.35),
    width: moderateScale(200, 0.4),
    borderRadius: moderateScale(24, 0.5),
    backgroundColor: '#fff',
    shadowColor: 'rgb(0, 122, 255)',
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
  },
  btnText: {
    paddingVertical: 7.5,
    fontSize: moderateScale(16, 0.5),
    color: '#0091ff',
    fontWeight: '600',
  },

  inputContainer: {
    width: '85%',
    marginBottom: moderateScale(10, 2.5),
    fontWeight: 'bold',
  },

  wrapper: {
    paddingTop: moderateScale(variables.isIphoneX ? 105 : 51, 0.5),
    paddingHorizontal: moderateScale(35, 2),
  },
  iconInput: {
    fontSize: moderateScale(20, 0.8),
    color: 'white',
    paddingRight: moderateScale(12, 0.5),
    position: 'relative',
    top: moderateScale(15, -0.5),
    bottom: 0,
  },
  iconButton: {
    fontSize: moderateScale(24, 0.5),
    color: '#007aff',
    position: 'absolute',
    right: 0,
  },
  text: {
    alignSelf: 'center',
    fontSize: moderateScale(22, 0.5),
    color: '#fdfeff',
    fontWeight: 'bold',
  },
  viewInput: { flexDirection: 'row', position: 'relative' },
  form: {
    paddingTop: moderateScale(25, 1),
  },
  spinner: { position: 'absolute', right: 16 },
});

const LoginForm = ({ login }) => {
  const [state, actions] = useLoginForm();
  const submit = async () => {
    actions.setSubmiting(true);
    await login({ username: state.username, password: state.password });
    actions.setSubmiting(false);
    Keyboard.dismiss();
  };

  return (
    <View style={styles.wrapper}>
      <Text uppercase style={styles.text}>
        mobifone e-office
      </Text>
      <Form style={styles.form}>
        <View style={styles.viewInput}>
          <Icon style={styles.iconInput} name="user" type="Feather" />
          <Input
            containerStyle={styles.inputContainer}
            placeholderTextColor="white"
            placeholder={state.usernamePlaceholder}
            value={state.username}
            onChangeText={actions.setUsername}
            onFocus={actions.setUsernamePlaceholder}
          />
        </View>

        <View style={styles.viewInput}>
          <Icon style={styles.iconInput} name="lock" type="Feather" />
          <Input
            containerStyle={styles.inputContainer}
            placeholderTextColor="white"
            secureTextEntry
            placeholder={state.passwordPlaceholder}
            value={state.password}
            onChangeText={actions.setPassword}
            onFocus={actions.setPasswordPlaceholder}
          />
        </View>
        <View style={{ flexDirection: 'row', width: '100%' }}>
          <Button block style={styles.btn} onPress={submit} disabled={state.submiting}>
            <Text style={styles.btnText} uppercase={false}>
              Đăng nhập
            </Text>
            {!state.submiting && (
              <Icon name="arrow-right" type="Feather" style={styles.iconButton} />
            )}
            {state.submiting && <Spinner size="small" color="#007aff" style={styles.spinner} />}
          </Button>
        </View>
      </Form>
    </View>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

export default LoginForm;
